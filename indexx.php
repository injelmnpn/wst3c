<?php
include('config.php');

if (isset($accessToken))
{
	if (!isset($_SESSION['facebook_access_token'])) 
	{
		$_SESSION['facebook_access_token'] = (string) $accessToken;
		
		$oAuth2Client = $fb->getOAuth2Client();
		
		$longLivedAccessToken = $oAuth2Client->getLongLivedAccessToken($_SESSION['facebook_access_token']);
		$_SESSION['facebook_access_token'] = (string) $longLivedAccessToken;
		
		$fb->setDefaultAccessToken($_SESSION['facebook_access_token']);
	} 
	else 
	{
		$fb->setDefaultAccessToken($_SESSION['facebook_access_token']);
	}
	

	if (isset($_GET['code'])) 
	{
		header('Location: ./');
	}
	
	
	try {
		$fb_response = $fb->get('/me?fields=name,first_name,last_name');
	
		$fb_user = $fb_response->getGraphUser();
		$_SESSION['fb_user_id'] = $fb_user->getProperty('id');
		$_SESSION['fb_user_name'] = $fb_user->getProperty('name');
		
		
	} catch(Facebook\Exceptions\FacebookResponseException $e) {
		echo 'Facebook API Error: ' . $e->getMessage();
		session_destroy();
		header("Location: ./");
		exit;
	} catch(Facebook\Exceptions\FacebookSDKException $e) {
		echo 'Facebook SDK Error: ' . $e->getMessage();
		exit;
	}
} 
else 
{	
	$fb_login_url = $fb_helper->getLoginUrl('http://localhost/Assignment2/');
}
?>

<?php
include('configg.php');
$login_button = '';


if(isset($_GET["code"]))
{
 $token = $google_client->fetchAccessTokenWithAuthCode($_GET["code"]);

 if(!isset($token['error']))
 {
  $google_client->setAccessToken($token['access_token']);
  $_SESSION['access_token'] = $token['access_token'];
  $google_service = new Google_Service_Oauth2($google_client);
  $data = $google_service->userinfo->get();

  if(!empty($data['given_name']))
  {
   $_SESSION['user_first_name'] = $data['given_name'];
  }

  if(!empty($data['family_name']))
  {
   $_SESSION['user_last_name'] = $data['family_name'];
  }

  if(!empty($data['email']))
  {
   $_SESSION['user_email_address'] = $data['email'];
  }

  if(!empty($data['gender']))
  {
   $_SESSION['user_gender'] = $data['gender'];
  }

  if(!empty($data['picture']))
  {
   $_SESSION['user_image'] = $data['picture'];
  }
 }
}


if(!isset($_SESSION['access_token']))
{

 $login_button = '<a href="'.$google_client->createAuthUrl().'" style="text-decoration: none; color: white;">Sign In with Google+</a>';
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Google/facebook log in</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>
<body>
<?php if(isset($_SESSION['fb_user_id'])): ?>

	<div class="container" style="margin-top:30px">
	  <div class="row">
		<div class="col-10">
			<div class="shadow p-5 mb-5 bg-body rounded">
				<div class="row align-items-start">
        			<div class="col-6">
            			<p class="fs-5">Name: <?php echo $_SESSION['fb_user_name']; ?></p>
        			</div>
        			<div class="col-6">
            			<p class="fs-5 text-end">March 03, 2022</p>
        			</div>
  				</div>
  				<div class="row align-items-start">
        			<div class="col-6">
            			<p class="fs-5">Year/Section: 3rd Year-Sec. C</p>
        			</div>
        			<div class="col-6">
            			<p class="fs-5 text-end">11:59 PM</p>
        			</div>
  				</div>
		  <br><br>
		  <center><b><p class="fs-4">WELCOME FACEBOOK</p></b></center>

		  <center>
				<div class="d-grid gap-2 col-6 mx-auto">
					<button class="btn btn-success" type="button"><a href="logout.php" style="text-decoration: none; color: white;">LOGOUT</a></button>
				</div>
			</center>
			</div>
		</div>
	  </div>
	</div>
<?php else: ?>
 
<br><br>
<div class="container"> 
    <div class="row align-items-start">
        <div class="col-6">
            <p class="fs-5">Name: Angel Joy B. Manipon</p>
        </div>
        <div class="col-5">
            <p class="fs-5">March 03, 2022</p>
        </div>
  </div>
  <div class="row align-items-start">
        <div class="col-6">
            <p class="fs-5">Year/Section: 3rd Year-Sec. C</p>
        </div>
        <div class="col-5">
            <p class="fs-5">11:59 PM</p>
        </div>
  </div>

  <div class="row">
    <div class="col-8">
        <div class="shadow p-5 mb-5 bg-body rounded">
        <center>
            <a href="<?php echo $fb_login_url; ?>"><button type="button" class="btn btn-primary" style="background-color: #3b5998;"><img src="124010.png" width="35" height="35"> Login with Facebook</button></a><br><br>

   			 <?php if($login_button == '')
   			{
    			echo '<h3><b>Name :</b> '.$_SESSION['user_first_name'].' '.$_SESSION['user_last_name'].'</h3>';
    			echo '<h3><a href="logout.php">Logout</h3></div>';
   			}
   			else
   			{
	   			echo '<button type="button" class="btn btn-danger" style="background-color: #dd4b39;"><img src="124011.png" width="35" height="35"> '.$login_button . '</button>';
   			}
   			?>
        </center>
        </div>
    </div>
  </div>
</div>
</body>
</html>
<?php endif ?>