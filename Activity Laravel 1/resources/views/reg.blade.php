<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Personal Information-AM</title>
    <link rel="icon" href="image/logo.png" type="image/gif/png">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<style>
   :root {
    --rm-primary: #FF4C00;
    --rm-dark: #121314;
    --rm-light: #FFF;
    --rm-muted: #7D7D7D;
    --rm-blue: #82CAFA;
    --rm-bluee: #B4CFEC;
    --rm-wel: #0669b2;
    --rm-come: #00a8ec;
    }
    img{
        margin-left: 40px;
    }
    a{
        text-decoration: none;
        color: rgba(34,54,69,.7);
        font-weight: 500;
    }
    a:hover{
        color: #ADD8E6;
    }
    ul{
        list-style-type: none;
    }
    nav{
      box-shadow: 0 0 10px rgba(0,0,0,0.8);
    }
    .navbar .navbar-brand a{
        padding: 1rem 0;
        text-decoration: none;
    }
    .navbar-toggler{
        background: #ADD8E6;
        border: none;
        padding: 10px 6px;
    }
    .navbar-toggler span{
        display: block;
        width: 22px;
        height: 2px;
        border: 1px;
        background: #fff;
    }
    .navbar-toggler span + span{
      margin-top: 4px;
      width: 18px;
      margin-left: 4px;
    }
    .navbar-toggler span + span + span {
      margin-top: 10px;
      margin-left: 1px;
    }
    .navbar-expand-lg .navbar-nav .nav-link{
        padding: 2rem 1.2rem;
        font-size: 1rem;
        position: relative;
    }
    .navbar-expand-lg .navbar-nav .nav-link:hover{
        border-top: 4px solid #ADD8E6;
    }
    .navbar-expand-lg .navbar-nav .nav-link:.active{
        border-top: 4px solid #ADD8E6;
        color: #ADD8E6;
    }
    .rm-hero {
    background: url(./assets/images/hero-1.jpg);
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    height: 70vh;
    }
    .img{
      margin-top: 100px;
      width: 350px;
      height: 350px;
    }
    .imagee{
      margin-left: 10px;
      margin-right: 10px;
      height: 570px;
    }
    .rm-bg-dark {
    background: var(--rm-light);
    }
    .rm-bg-light {
    background: var(--rm-bluee);
    }
    .rm-hero {
    background: url(image/gif2.jpg);
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    height: 86vh;
    margin-top: 20px;
    }
    .rm-heroo {
    background: url(image/gif.webp);
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    height: 80vh;
    }
    .rm-display-7 {
    font-size: 7rem;
    font-weight: 700;
    margin-top: 10px;
    }
    .rm-text-light {
    font-size: 20px;;
    color: var(--rm-blue);
    }
    .rm-text-primary {
    color: var(--rm-dark);
    }
    .rm-display-3 {
    font-size: 5rem;
    font-weight: 700;
    color: var(--rm-dark);
    }
    .rm-display-9 {
    font-size: 5rem;
    font-weight: 700;
    color: var(--rm-blue);
    }
    .rm-display-2 {
    font-family: 'san-serif';
    font-size: 60px;
    font-weight: 700;
    margin-top: 150px;
    }
    .rm-display-4 {
    font-size: 3rem;
    font-weight: normal;
    color: var(--rm-light);
    font-family: Arial, Helvetica, sans-serif;
    }
    .rm-display-0 {
    font-size: 1rem;
    font-weight: normal;
    color: var(--rm-dark);
    font-family: 'sans-serif';
    font-style: italic;
    }
    .rm-display-11 {
    font-size: 1rem;
    font-weight: normal;
    color: var(--rm-dark);
    font-family: Arial, Helvetica, sans-serif;
    }
    .rm-display-12 {
    font-size: 3rem;
    font-weight: normal;
    color: var(--rm-dark);
    font-family: Arial, Helvetica, sans-serif;
    }
    .card-img-top{
      margin-left: 0;
    }
    .footer-copyright {
    background: var(--rm-bluee);
    }
    .card-titlee{
      margin-top: 41px;
    }
    .foot{
      margin-bottom: 13px;
    }
    .card{
      margin-top: 20px;
      margin-left: 20px;
      margin-right: 20px;
    }
    .cardd{
      margin-top: 20px;
      margin-left: 20px;
      margin-right: 20px;
    }
    .carddd{
      margin: 0 auto; /* Added */
        float: none; /* Added */
        margin-bottom: 10px; /* Added */
    }
    h3{
      font-size: 22px;
      font-family: Arial, Helvetica, sans-serif;
    }
    .main-wrap{
    width: 100%;
    height: 100vh;
    display: flex;
    justify-content: center;
    align-items: center;
    }
    .box-container{
    display: flex;
    background: #fff;
    }
    .img-box{
    width: 100px;
    }
    .form-wrap{
    width: 50%;
    height: 100%;
    padding: 20px;
    }
    .form-wrap .top-signup{
    text-align: right;
    padding-top: 10px;
    font-size: 13px;
    font-weight: 400;
    color: rgb(155, 152, 152);  
    }
    .form-wrap .signup-btn{
    text-decoration: none;
    color: rgb(155, 152, 152);
    border: 1px solid rgb(182, 181, 181);
    padding: 4px 8px;
    font-size: 12px;
    margin-left: 8px;
    border-radius: 20px;
    }
    .form-wrap .signup-btn:hover{
    background-color: rgba(238, 238, 238, 0.527);
    }
    .form-wrap .mid-container{
    padding-top: 40px;
    padding-left: 40px;
    }
    .mid-container h6{
    font-size: 16px;
    font-weight: 400;
    padding: 8px 2px;
    letter-spacing: 0.002rem;
    color: rgb(155, 152, 152);
    }
    .mid-container .form{
    padding-top: 26px;
    width: 100px;
    }
    .mid-container .form label{
    font-weight: 500;
    }
    .mid-container .form input{
    width: 320px;
    margin: 6px 0px;
    height: 33px;
    color: rgb(155, 152 152);
    padding-left: 5px;
    border-radius: 4px;
    border: 1px solid rgb(216, 215, 215);
    }
    .mid-container .form input:focus{
    border: 1px solid rgb(182, 180, 180);
    outline: none;
    }
    .mid-container .form input::placeholder{
    color: rgb(175, 174, 174);
    }
    .fg-pass{
    color: rgb(175, 174, 174);
    font-size: 14px;
    text-decoration: none;
    }
    .mid-container .form .login-btn{
    padding: 10px;
    cursor: pointer;
    width: 140px;
    font-size: 16px;
    margin-top: 20px;
    background-color: rgb(68, 138, 230);
    color: rgb(247, 244, 244);
    border-radius: 20px;
    border: none;
    }
    .mid-container .form .login-btn a{
    text-decoration: none;
    color: white;
    }
    .mid-container .form .login-btn:hover{
    background-color: rgba(39, 39, 175, 0.938);
    transition: all ease 1s;
    }
    .login-with{
    padding: 50px 40px;
    }
    .login-with span{
    padding: 8px;
    width: 25px;
    height: 25px;
    margin: 4px 5px;
    }
    .login-with a img{
    width: 25px;
    height: 25px;
    margin-bottom: -3px;
    }
    .register{
    background: -webkit-linear-gradient(left, #3931af, #00c6ff);
    margin-top: 3%;
    padding: 3%;
}
.register-left{
    text-align: center;
    color: #fff;
    margin-top: 4%;
}
.register-left input{
    border: none;
    border-radius: 1.5rem;
    padding: 2%;
    width: 60%;
    background: #f8f9fa;
    font-weight: bold;
    color: #383d41;
    margin-top: 30%;
    margin-bottom: 3%;
    cursor: pointer;
}
.register-right{
    background: #f8f9fa;
    border-top-left-radius: 10% 50%;
    border-bottom-left-radius: 10% 50%;
}
@-webkit-keyframes mover {
    0% { transform: translateY(0); }
    100% { transform: translateY(-20px); }
}
@keyframes mover {
    0% { transform: translateY(0); }
    100% { transform: translateY(-20px); }
}
.register-left p{
    font-weight: lighter;
    padding: 12%;
    margin-top: -9%;
}
.register .register-form{
    padding: 10%;
}
.btnRegister{
    float: right;
    margin-top: 10%;
    border: none;
    border-radius: 1.5rem;
    padding: 2%;
    background: #0062cc;
    color: #fff;
    font-weight: 600;
    width: 50%;
    cursor: pointer;
}
.register .nav-tabs{
    margin-top: 3%;
    border: none;
    background: #0062cc;
    border-radius: 1.5rem;
    width: 28%;
    float: right;
}
.register .nav-tabs .nav-link{
    padding: 2%;
    height: 34px;
    font-weight: 600;
    color: #fff;
    border-top-right-radius: 1.5rem;
    border-bottom-right-radius: 1.5rem;
}
.register .nav-tabs .nav-link:hover{
    border: none;
}
.register .nav-tabs .nav-link.active{
    width: 100px;
    color: #0062cc;
    border: 2px solid #0062cc;
    border-top-left-radius: 1.5rem;
    border-bottom-left-radius: 1.5rem;
}
.register-heading{
    text-align: center;
    margin-top: 8%;
    margin-bottom: -15%;
    color: #495057;
}
</style>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container-fluid">
    <a class="navbar-brand" href="#"><img src="image/logo.png" width="70" heigth="70"></a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span></span>
      <span></span>
      <span></span>
    </button>
    <div class="collapse navbar-collapse jstify-content-between" id="navbarSupportedContent">
      <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link" href="/">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/about">About</a>
        </li>
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="#">Registration</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/login">Login</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/contact">Contact</a>
        </li>
      </ul>
      <ul class="navbar-nav ms-auto">
    
      </ul>
    </div>
  </div>
</nav>

<div class="card">
  <div class="card-body">
          <h3 class="rm-display-4 text-center text-dark">REGISTER NOW</h3>
          <h1 class="rm-display-0 text-center rm-text-light">don't waste time</span></h1>
  </div>
</div>

<div class="container register">
  <div class="row">
    <div class="col-md-3 register-left">
      <h3>Welcome</h3>
      <p>Register now, there always a time</p>
      <a href="/"><input type="submit" name="" value="Login"/></a><br/>
    </div>

      <div class="col-md-9 register-right">
        <div class="tab-content" id="myTabContent">
          <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
            <div class="row register-form">

              <div class="card">
                <div class="card-body">
                    <div class="container">

                      <div class="row">
                          <div class="col-md-4">
                            <label for="inputlastname" class="form-label">Lastname:</label>
                            <input type="lname" class="form-control" id="inputlastname" name="lastname" required>
                          </div>

                          <div class="col-md-4">
                            <label for="inputfirstname" class="form-label">Firstname:</label>
                            <input type="fname" class="form-control" id="inputfisrtname" name="firstname" required>
                          </div>

                          <div class="col-md-4">
                          <label for="inputSex" class="form-label">Sex</label>
                              <select id="inputSex" class="form-select" name="sex" required>
                                  <option selected>Female</option>
                                  <option>Male</option>
                              </select>
                          </div>
                        </div>

                        <br>
                        <div class="row">
                          <div class="col-md-10">
                          <label for="inpucivilstatus" class="form-label">Interest</label>
                              <select id="inpucivilstatus" class="form-select" name="civil" required>
                                  <option selected>Web Design</option>
                                  <option>Graphic Design</option>
                                  <option>Software Eng.</option>
                                  <option>UI Design</option>
                              </select>
                          </div>
                        </div>

                        <br>
                        <div class="row">
                          <div class="col-10">
                            <label for="inputAddress" class="form-label">Address:</label>
                            <input type="text" class="form-control" id="inputAddress" name="address" placeholder="no, street, barangay, town/city, province" required>
                          </div>
                        </div>

                        <br>
                        <div class="row">
                          <div class="col-10">
                            <label for="inputemail" class="form-label">Email Address:</label>
                            <input type="text" class="form-control" id="inputmail" name="email" placeholder="@gmail.com" required>
                          </div>
                        </div>

                        <br>
                        <div class="row">
                          <div class="col-10">
                            <label for="inputnumber" class="form-label">Mobile No.</label>
                            <input type="text" class="form-control" id="inputnumber" name="mobile" required>
                          </div>
                        </div>

                          <div class="col-12">
                          <br>
                            <button type="submit" class="btn btn-primary" name="output" value="output"><a href="/" style="color: white;">Submit Form</a></button>
                          </div>

                      </div>
                    </div>
                  </div>
             
            </div>
          </div>
        </div>
      </div>

  </div>
</div>


<br><br>
<footer class="page-footer font-small blue pt-4">
  <div class="footer-copyright text-center py-3">© 2022 Copyright:
    <a href="/"> Designed by: Angel Joy B. Manipon | Power by <b>Laravel</b></a>
  </div>
</body>
</html>
