<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Personal Information-AM</title>
    <link rel="icon" href="image/logo.png" type="image/gif/png">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <style>
  :root {
    --rm-primary: #FF4C00;
    --rm-dark: #121314;
    --rm-light: #FFFFFF;
    --rm-muted: #7D7D7D;
    --rm-blue: #82CAFA;
    --rm-bluee: #B4CFEC;
    --rm-wel: #0669b2;
    --rm-come: #00a8ec;
    }
    img{
        margin-left: 40px;
    }
    a{
        text-decoration: none;
        color: rgba(34,54,69,.7);
        font-weight: 500;
    }
    a:hover{
        color: #ADD8E6;
    }
    ul{
        list-style-type: none;
    }
    nav{
      box-shadow: 0 0 10px rgba(0,0,0,0.8);
    }
    .navbar .navbar-brand a{
        padding: 1rem 0;
        text-decoration: none;
    }
    .navbar-toggler{
        background: #ADD8E6;
        border: none;
        padding: 10px 6px;
    }
    .navbar-toggler span{
        display: block;
        width: 22px;
        height: 2px;
        border: 1px;
        background: #fff;
    }
    .navbar-toggler span + span{
      margin-top: 4px;
      width: 18px;
      margin-left: 4px;
    }
    .navbar-toggler span + span + span {
      margin-top: 10px;
      margin-left: 1px;
    }
    .navbar-expand-lg .navbar-nav .nav-link{
        padding: 2rem 1.2rem;
        font-size: 1rem;
        position: relative;
    }
    .navbar-expand-lg .navbar-nav .nav-link:hover{
        border-top: 4px solid #ADD8E6;
    }
    .navbar-expand-lg .navbar-nav .nav-link:.active{
        border-top: 4px solid #ADD8E6;
        color: #ADD8E6;
    }
    .rm-hero {
    background: url(./assets/images/hero-1.jpg);
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    height: 70vh;
    }
    .img{
      margin-top: 50px;
      width: 270px;
      height: 270px;
      position: center;
    }
    .imagee{
      margin-left: 10px;
      margin-right: 10px;
      height: 570px;
    }
    .rm-bg-dark {
    background: var(--rm-light);
    }
    .rm-bg-light {
    background: var(--rm-bluee);
    }
    .rm-hero {
    background: url(image/img2.png);
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    height: 86vh;
    margin-top: 200px;
    }
    .rm-heroo {
    background: url(image/back.jpg);
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    height: 86vh;
    margin-top: 20px;
    }
    .rm-herooo {
    background: url(image/hoobby.png);
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    height: 86vh;
    margin-top: 20px;
    }
    .rm-her {
    background: url(image/palette.jpg);
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    height: 86vh;
    margin-top: 20px;
    }
    .rm-display-1 {
    font-size: 7rem;
    font-weight: 700;
    margin-top: 4px;
    
    }
    .rm-display-7 {
    font-size: 7rem;
    font-weight: 700;
    margin-top: 10px;
    }
    .rm-text-light {
    color: var(--rm-light);
    }
    .rm-text-primary {
    color: var(--rm-dark);
    margin-left: 10px;
    }
    .rm-display-3 {
    font-size: 3rem;
    font-weight: 700;
    font-family: Arial, Helvetica, sans-serif;
    font-style: italic;
    }
    .rm-display-13 {
    font-size: 3rem;
    font-weight: 700;
    font-family: Arial, Helvetica, sans-serif;
    font-style: italic;
    color: var(--rm-blue);
    }
    .rm-display-2 {
    font-family: 'san-serif';
    font-size: 60px;
    font-weight: 700;
    margin-top: 150px;
    }
    .rm-display-4 p{
    font-size: 16px;
    font-weight: normal;
    color: var(--rm-light);
    }
    .rm-display-9{
    font-size: 60px;
    font-weight: normal;
    color: var(--rm-blue);
    }
    .rm-display-22 {
    font-family: 'san-serif';
    font-size: 40px;
    font-weight: 700;
    margin-top: 150px;
    font-style: italic;
    }
    .rm-display-44{
    font-size: 20px;
    font-weight: normal;
    color: var(--rm-light);
    }
    .rm-display-5 {
    font-size: 12px;
    font-weight: normal;
    color: var(--rm-light);
    }
    .card-img-top{
      margin-left: 0;
    }
    .footer-copyright {
    background: var(--rm-bluee);
    }
    .card-titlee{
      margin-top: 41px;
    }
    .foot{
      margin-bottom: 13px;
    }
    hr{
      color: var(--rm-bluee);
      height: 20px;
    }
</style>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container-fluid">
    <a class="navbar-brand" href="#"><img src="image/logo.png" width="70" heigth="70"></a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span></span>
      <span></span>
      <span></span>
    </button>
    <div class="collapse navbar-collapse jstify-content-between" id="navbarSupportedContent">
      <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link" href="/">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="#">About</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/reg">Registration</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/login">Login</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/contact">Contact</a>
        </li>
      </ul>
      <ul class="navbar-nav ms-auto">
      </ul>
    </div>
  </div>
</nav>

<div class="container">
<div class="container-fluid rm-heroo d-flex flex-column justify-content-center">
  <div class="row align-items-center">
    <div class="col">
    <div class="container-fluid rm-hero d-flex flex-column justify-content-center">
    </div>
    </div>
    <div class="col">
    <h1 class="rm-display-1 text-left rm-text-light">About<span class="rm-display-3 rm-text-primary">Me</span></h1>
      <h3 class="rm-display-4 text-left text-dark">
        <p>Hi! Friend, I'm Angel Joy B. Manipon, I'm currently BSIT-3C student from Pangasinan State University Urdaneta City-Campus.
          I'm 20 years old, I was born on April 18, 2001 in Dagupan City, Pangasinan.  I'm a simple girl but I have a wonderful dreams.
          My dreams is to become a Website Designer, User Interface (UI) Designer and Graphic Designer.</p>
    </div>
  </div>
</div>
</div>
<br><br><br><br><br>
<div class="container p-5">
    <div class="row">
      <div class="col p-xl-5">
        <h3 class="rm-display-3 rm-text-dark mt-5">MY <span class="rm-display-9 rm-text-primary">STORY</span> </h3>
          <p class="rm-text-muted rm-text-medium">This is my story when I was little child my dream is to become a dancer and singer. I was lovely person, annoying and joyful when I was a child. When I was a teen my dream is to be a dancer and a doctor. 
            When I was a teen I am a cry baby, immature, and joyful still, little get annoyed and I am friendly.
            When I now an adult everything got change now I want to become a website designer and a graphic designer. Dancing and Singing is now my hobby. Now I am an adult I am mature and contented but not everything.</p>
    </div>
    <div class="col"><img src="image/img3.jpg" alt="" class="img"></div>
</div>

<br><br><br><br>

<div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
  <div class="carousel-indicators">
    <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
    <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
    <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
  </div>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="image/img5.jpg" class="d-block w-100" alt="...">
      <div class="carousel-caption d-none d-md-block">
        <h5>Little One</h5>
        <p>Innocent and Lovely Child.</p>
      </div>
    </div>
    <div class="carousel-item">
      <img src="image/img2.jpg" class="d-block w-100" alt="...">
      <div class="carousel-caption d-none d-md-block">
        <h5>Dreaming</h5>
        <p>At the first place I don't know I want to be in the futures. Dream is FREE</p>
      </div>
    </div>
    <div class="carousel-item">
      <img src="image/img1.jpg" class="d-block w-100" alt="...">
      <div class="carousel-caption d-none d-md-block">
        <h5>Website Designer</h5>
        <p>I want to and I love to</p>
      </div>
    </div>
  </div>
  <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Previous</span>
  </button>
  <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Next</span>
  </button>
</div>



<br><br><br><br><br>
<hr style="color:  #B4CFEC; height: 10px;">
<br><br>
<div class="card">
  <div class="card-body">
      <h3 class="rm-display-13 text-center text-dark">Resume</span> </h3>
  </div>
</div>
<br><br><br><br>

<div class="container">
  <div class="row align-items-center">
  <div class="container-fluid rm-he d-flex flex-column justify-content-center">
      <div class="container-fluid d-flex flex-column justify-content-center">
        <img src="image/resume.jpg" class="rounded mx-auto d-block" alt="" width="600" height="800">
      </div>
    </div>
  </div>
</div>
</div>


<h1 class="rm-display-22 text-center text-dark">I Hope You Know Me Better My Friends</h1>
<h3 class="rm-display-44 text-center text-dark">I'm the kind of person who's different at home, different from school, 
I'm different in person, and I'm different online. </h3>

<br><br><br><br><br><br><br><br><br><br>
<footer class="page-footer font-small blue pt-4">
  <div class="container-fluid text-center text-md-left">
    <div class="row justify-content-center" style="background: var(--rm-blue);">
      <div class="col-2">
            <br>
            <a href="#!"><img src="image/fb.png" width="30" heigth="30"></a>
      </div>

      <div class="col-2">
            <br>
            <a href="#!"><img src="image/google.png" width="30" heigth="30"></a>
      </div>

      <div class="col-2">
            <br>
            <a href="#!"><img src="image/instagram.png" width="30" heigth="30" class="foot"></a>
      </div>
    </div>
  </div>

  <div class="footer-copyright text-center py-3">© 2022 Copyright:
    <a href="/"> Designed by: Angel Joy B. Manipon | Power by <b>Laravel</b></a>
  </div>
</footer>
</body>
</html>
